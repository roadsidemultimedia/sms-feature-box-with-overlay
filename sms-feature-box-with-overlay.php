<?php
  /*
Plugin Name: Feature Box with Overlay 
Plugin URI: http://www.roadsidemultimedia.com
Description: Feature Box with Overlay
Author: Curtis Grant
PageLines: true
Version: 1.0
Section: true
Class Name: FeatureBoxwithOverlay
Filter: component, custom
Loading: active
Text Domain: dms-sms
Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-feature-box-with-overlay
Bitbucket Branch: master
*/
if( ! class_exists( 'PageLinesSectionFactory' ) )
  return;

class FeatureBoxwithOverlay extends PageLinesSection {

  function section_styles(){
    
    wp_enqueue_style( 'fbwo', $this->base_url.'/css/fbwo.css');
  }
  function section_persistent(){
    
  }

  function section_opts(){

    $opts = array(
      array(
            'type'  => 'image_upload',
            'key' => 'fbwo_img',
            'label' => __( 'Front Image', 'pagelines' ),
            'has_alt' => false,
            'col' => 1,
            ),
      array(
            'type'  => 'text',
            'key' => 'fbwo_link',
            'label' => __( 'Link', 'pagelines' ),
            'has_alt' => false,
            'col' => 1,
            ),
      array(
            'type'  => 'textarea',
            'key' => 'fbwo_text',
            'label' => __( 'Heading', 'pagelines' ),
            'has_alt' => false,
            'col' => 2,
            ),
    );

    return $opts;

  }

  /**
  * Section template.
  */
   function section_template( $location = false ) {

    // Global Variables (pl_setting)
    $fbwoimg = ( $this->opt('fbwo_img') ) ? $this->opt('fbwo_img') : $this->base_url.'/img/couple.jpg';
    $fbwolink = ( $this->opt('fbwo_link') ) ? $this->opt('fbwo_link') : "#";
    $fbwotext = ( $this->opt('fbwo_text') ) ? $this->opt('fbwo_text') : "header";
  ?>
  <div class="sms-feature-box-with-overlay"><figure>
  <a href="<?php echo $fbwolink; ?>">
    <img src="<?php echo $fbwoimg; ?>" alt=""> 
    <figcaption>
      <span class="triangle"></span>
      <h3 class="fw-400 ls-2 fs-16 uppercase padding-small"><?php echo $fbwotext; ?></h3>
    </figcaption>
  </a>
</figure>

<?php 
}

}


